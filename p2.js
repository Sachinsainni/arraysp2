let detail = [{ "id": 1, "first_name": "Valera", "last_name": "Pinsent", "email": "vpinsent0@google.co.jp", "gender": "Male", "ip_address": "253.171.63.171" },
{ "id": 2, "first_name": "Kenneth", "last_name": "Hinemoor", "email": "khinemoor1@yellowbook.com", "gender": "Polygender", "ip_address": "50.231.58.150" },
{ "id": 3, "first_name": "Roman", "last_name": "Sedcole", "email": "rsedcole2@addtoany.com", "gender": "Genderqueer", "ip_address": "236.52.184.83" },
{ "id": 4, "first_name": "Lind", "last_name": "Ladyman", "email": "lladyman3@wordpress.org", "gender": "Male", "ip_address": "118.12.213.144" },
{ "id": 5, "first_name": "Jocelyne", "last_name": "Casse", "email": "jcasse4@ehow.com", "gender": "Agender", "ip_address": "176.202.254.113" },
{ "id": 6, "first_name": "Stafford", "last_name": "Dandy", "email": "sdandy5@exblog.jp", "gender": "Female", "ip_address": "111.139.161.143" },
{ "id": 7, "first_name": "Jeramey", "last_name": "Sweetsur", "email": "jsweetsur6@youtube.com", "gender": "Genderqueer", "ip_address": "196.247.246.106" },
{ "id": 8, "first_name": "Anna-diane", "last_name": "Wingar", "email": "awingar7@auda.org.au", "gender": "Agender", "ip_address": "148.229.65.98" },
{ "id": 9, "first_name": "Cherianne", "last_name": "Rantoul", "email": "crantoul8@craigslist.org", "gender": "Genderfluid", "ip_address": "141.40.134.234" },
{ "id": 10, "first_name": "Nico", "last_name": "Dunstall", "email": "ndunstall9@technorati.com", "gender": "Female", "ip_address": "37.12.213.144" }]



1.

const output1 = detail.filter(x => x.gender == "Agender");
console.log(output1);

2.

const output2 = detail.map(function (x) {
    x.component = x.ip_address.split(".");
    return x;
})
console.log(output2);

3.

const output3 = detail.reduce(function (acc, curr) {
    acc += parseInt(curr.component[1]);
    return acc;

}, 0)
console.log(output3);


3.2

const output3_2 = detail.reduce(function (acc, curr) {
    acc += parseInt(curr.component[3]);
    return acc;

}, 0)
console.log(output3_2);


4.

const output4 = detail.map(function (x) {
    x.full_name = x.first_name + " " + x.last_name;
    return x.full_name;

})
console.log(output4);

5.

const output5 = detail.filter(function (x) {
    let org = x.email.endsWith(".org")
    return org;

})
console.log(output5);

6.

const output6 = detail.filter(function (x) {
    if (x.email.slice(-4) == ".org" || x.email.slice(-4) == ".com" || x.email.slice(-3) == ".au") {
        return x.email;
    }

})
console.log(output6.length)




